from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, TodoItemForm

# Create your views here.


def todo_list_list(request):
    todo_lists = TodoList.objects.all()
    context = {"todo_list": todo_lists}
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todo_details = get_object_or_404(TodoList, id=id)
    context = {
        "todo_details": todo_details,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            todo_list = form.save()
            todo_list.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = TodoForm()

    context = {
        "form": form,
    }

    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    todo_tasks = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=todo_tasks)
        if form.is_valid():
            todo_tasks = form.save()
            return redirect("todo_list_detail", id=todo_tasks.id)
    else:
        form = TodoForm(instance=todo_tasks)

    context = {"form": form}

    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    todo_tasks = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo_tasks.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todo_items = form.save()
            todo_items.save()
            return redirect("todo_list_detail", id=todo_items.list.id)
    else:
        form = TodoItemForm()

    context = {
        "form": form,
    }

    return render(request, "todos/item_create.html", context)


def todo_item_update(request, id):
    todo_items = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo_items)
        if form.is_valid():
            todo_items = form.save()
            return redirect("todo_list_detail", id=todo_items.list.id)
    else:
        form = TodoItemForm(instance=todo_items)

    context = {
        "form": form,
    }

    return render(request, "todos/item_update.html", context)
